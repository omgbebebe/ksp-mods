{ pkgs ? import <nixpkgs> {} }:

with pkgs;

stdenv.mkDerivation rec {
  name = "ksp-realisim";
  src = ./stub;
  mods = (with kspMods; [
    RP0-Pap-TechTree
  ]);
  buildInputs = [ libstdcxx5 libcxxabi libcxx ] ++ (mods);

  shellHook = ''
    for m in $mods; do
      modName=$(ls $m/GameData)
      rm -rf GameData/$modName
      rsync --chown=scor:users --chmod=D770,F660 -rptlog $m/GameData/* GameData/
    done
#      LD_LIBRARY_PATH="/nix/store/1s3gzyn5yr21vhdrrc56lgasrs2ryjq1-gcc-5.4.0-lib/lib/:$LD_LIBRARY_PATH"
    cat >start1.sh <<EOF
      #!/usr/bin/env bash
      steam-run bash -c 'LD_LIBRARY_PATH="${libstdcxx5}/lib/:\$LD_LIBRARY_PATH" LD_PRELOAD="${libstdcxx5}/lib/" LD_PRELOAD="${libcxx}/lib/libc++.so.1" LD_PRELOAD="${libcxxabi}/lib/libc++abi.so.1" LC_ALL="C" ./KSP.x86_64'
EOF
    chmod +x ./start1.sh
  '';
} 
