{ stdenv, fetchzip, fetchFromGitHub, kspMod }:

let
  mname = "ModuleManager";

  src = rec {
    version = "448a324efa30c4f47f9198c68982e0df5ada5bc9";
    src = fetchFromGitHub {
      owner = "sarbian";
      repo = mname;
      rev = version;
      sha256 = "2rmq2bc5wbj42gqwx06622fk369h81jdpqrif5da1k1iwr4rni9x";
    };
    packageType = "src";
    pluginsDir = "./ModularFlightIntegrator/";
    #gamedataDir = null;
  };

  bin = rec {
    #version = "2.8.0";
    version = "2.7.6";
    src = fetchzip {
      url = "https://ksp.sarbian.com/jenkins/job/ModuleManager/126/artifact/ModuleManager-2.7.6.zip";
      #sha256 = "1y2rcmby99kcqqfdh8lwy1w4aphad3bg0bj58pp8sf3w8xxm5268";
      sha256 = "0rkrsspbwhvgx9q2l272w1i7alsdwdk96yjphww2lnn3ikasikad";
      stripRoot=false;
    };

    pluginsDir = "./";
    pluginsDst = "./GameData/";
  };
in kspMod ({
  inherit mname; 
} // bin)
