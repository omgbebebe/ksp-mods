{ pkgs, stdenv, fetchzip, fetchFromGitHub, kspMod }:

{ texRes ? "2K"
} @ args:

kspMod rec {
  mname = "RSS-Textures";
  version = "10.4";
  requiredMods = [ ];
  src = fetchzip {
    url = "https://github.com/KSP-RO/RSS-Textures/releases/download/v10.4/4096.zip";
    sha256 = "1fi2lk86a2g90qzx51gri1bzs1kpj290xm72xwrxxh62prwaym95";
    stripRoot = false;
  };
  gamedataDir = "GameData/${mname}/";
}
