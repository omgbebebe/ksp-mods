{ pkgs, stdenv, fetchzip, fetchFromGitHub, kspMod }:

let
  mname = "MechJeb2";
  # requiredMods = with pkgs.kspMods; [ ModularFlightIntegrator ];
  requiredMods = [];

  bin = rec {
    version = "2.6.0";
    src = fetchzip {
      url = "https://ksp.sarbian.com/jenkins/job/MechJeb2-Release/10/artifact/MechJeb2-2.6.0.0.zip";
      sha256 = "0z7dj0kvpv7986fqydg38xp5fphrnd3kz16n0wb8hyars81mm970";
      stripRoot = false;
    };

    pluginsDir = "${mname}/Plugins";
    gamedataDir = "${mname}/";
  };
in kspMod ({
  inherit mname; 
  inherit requiredMods;
} // bin)
