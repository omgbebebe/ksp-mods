{ pkgs ? import <nixpkgs> {} }:

with pkgs;

let
  version = "0.51";
in rec {
  build = dotnetenv.buildSolution rec {
    name = "RP-0-${version}";
    src = fetchzip {
      url = "https://github.com/KSP-RO/RP-0/releases/download/v${version}/RP-0-v${version}.zip";
      sha256 = "1iz0dcxml6ih8aq37p71hfifvcywm94sqgzqz5zzkyg9b7bcg4i7";
      stripRoot = false;
    };
    slnFile = "RP0.sln";
    assemblyInputs = [ dotnetenv.assembly20Path ]; 
  };
}
#in build
