#!/usr/bin/env bash

set -euo pipefail

#libs=("Assembly-CSharp.dll" "Assembly-CSharp-firstpass.dll" "UnityEngine.dll" "UnityEngine.UI.dll")

function usage {
  echo "usage: $0 <path_to_KSP_managed_folder>"
  echo "example: $0 ~/.steam/steam/steamapps/common/Kerbal\ Space\ Program/KSP_Data/Managed/"
}

if [[ -z ${1+x} ]]; then
  echo "library path is not defined!" >&2
  usage
  exit 1
else
  path=$1
  tmp=$(mktemp -d)
  pushd "$path"
  for l in *.dll; do
    echo "copy $l"
    cp "$l" $tmp/
  done
  echo "make libs archive"
  popd
  pushd $tmp
  zip ksp_libs.zip ./*
  popd
  mv ${tmp}/ksp_libs.zip ./
  rm -rf ${tmp}
fi

echo "done"
