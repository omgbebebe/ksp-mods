#{ pkgs ? import <nixpkgs> {} }:
{ stdenv, buildFHSUserEnv, fetchFromGitHub, unzip, git, perl, pkgs ? <nixpkgs> }:

with pkgs;
with stdenv;

let
  deps = [
#    self.RealismOverhaul
#    ctt
#    sxt
#    vspr
#    cc
#    cbk
#    dr
  ];
in rec {
  rp0 = mkDerivation rec {
    version = "0.51";
    name = "RP-0-${version}";
    src = fetchFromGitHub {
      owner = "KSP-RO";
      repo = "RP-0";
      rev = "master";
      sha256 = "03rkx9ydwk2fzaslp1hn19fsgvxgkr1s0ccnasalj225i6nh72k7";
    };
    outputs = [ "out" "GameData" ];

    ksp_libs_path = ksp-libs.out;
    buildInputs = [ ksp-libs git perl mono rsync pkgconfig perlPackages.YAMLLibYAML perlPackages.IPCSystemSimple ];
#    builder = ./build_sources.sh;
    patchPhase = ''
      mkdir -p $PWD/.bin
      echo "rm -f $@" > $PWD/.bin/del
      chmod +x $PWD/.bin/del
      export PATH="$PWD/.bin:$PATH"

      sed -i -re 's#<HintPath>.*.*Managed.([-.a-zA-Z0-9]*\.dll)#<HintPath>${ksp-libs.out}/\1#' Source/*.csproj
      cat Source/RP0.csproj
    '';
    buildPhase = ''
      pushd Source
      xbuild /p:Configuration=Release /p:Platform=x64
      popd
      substituteInPlace bin/yml2mm --replace /usr/bin/perl ${perl}/bin/perl
      substituteInPlace bin/makeversion --replace /usr/bin/perl ${perl}/bin/perl
      bin/yml2mm
    '';
    installPhase = ''
#      source ${stdenv}/setup
#      runHook preInstall

      pwd
      ls -la ./
      env
      echo "installing to $out"
      mkdir -p $out
      mkdir -p $GameData
      cp -r GameData $GameData/
      cp README.md $GameData
      cp LICENSE.md $GameData
      ln -s $GameData/GameData $out/
      ln -s $GameData/README.md $out/
      ln -s $GameData/LICENSE.md $out/

#      runHook postInstall
    '';
  };
  rp0fhs = buildFHSUserEnv {
    name = "RP-0";
    targetPkgs = pkgs: [ rp0 ];
    multiPkgs = pkgs: [ git perl unzip ];
    runScript = "bash";
  };
}
#in
#  rp0
