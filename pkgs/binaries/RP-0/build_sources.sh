#!/usr/bin/env bash
set -eo pipefail

source $stdenv/setup
PATH=$unzip/bin:$perl/bin:$git/bin:${out}/.bin:${pkgconfig}/bin:$PATH

ls -la ${src}
cd ${src}

echo "Patching..."
mkdir -p $PWD/.bin
echo "rm -f $@" > $PWD/.bin/del
chmod +x $PWD/.bin/del
export PATH="$PWD/.bin:$PATH"

sed -i -re 's#<HintPath>.*.*Managed.([-.a-zA-Z0-9]*\.dll)#<HintPath>${ksp-libs.out}/\1#' Source/*.csproj

echo "Building..."
pushd Source
xbuild /p:Configuration=Release /p:Platform=x64
popd
substituteInPlace bin/yml2mm --replace /usr/bin/perl ${perl}/bin/perl
substituteInPlace bin/makeversion --replace /usr/bin/perl ${perl}/bin/perl
bin/yml2mm

echo "Installing..."
ls -la ./
env
mkdir -p ${out}
mkdir -p ${GameData}

cp -r gamedata/* ${GameData}
cp readme.md ${GameData}
cp licence.md ${GameData}

cp -r gamedata/* ${out}
cp readme.md ${out}
cp licence.md ${out}
