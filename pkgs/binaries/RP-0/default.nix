{ stdenv, buildFHSUserEnv, fetchzip, unzip, git, perl, pkgs ? <nixpkgs> }:
#{ pkgs ? import <nixpkgs> {} }:

with pkgs;
with stdenv;

let
  deps = [
#    self.RealismOverhaul
#    ctt
#    sxt
#    vspr
#    cc
#    cbk
#    dr
  ];
in rec {
  rp0 = mkDerivation rec {
    version = "0.51";
    name = "RP-0-${version}";
    src = fetchzip {
      url = "https://github.com/KSP-RO/RP-0/releases/download/v${version}/RP-0-v${version}.zip";
      sha256 = "1iz0dcxml6ih8aq37p71hfifvcywm94sqgzqz5zzkyg9b7bcg4i7";
      stripRoot = false;
    };

    buildInputs = [ git perl unzip ];
    builder = ./build.sh;
  };
  rp0fhs = buildFHSUserEnv {
    name = "RP-0";
    targetPkgs = pkgs: [ rp0 ];
    multiPkgs = pkgs: [ git perl unzip ];
    runScript = "bash";
  };
}
#in
#  rp0
