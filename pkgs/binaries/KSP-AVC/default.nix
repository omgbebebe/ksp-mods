{ pkgs, stdenv, fetchzip, fetchFromGitHub, kspMod }:

kspMod rec {
  mname = "KSP-AVC";
  version = "1.1.6.2";
  requiredMods = [ ];
  src = fetchzip {
    url = "https://github.com/CYBUTEK/KSPAddonVersionChecker/releases/download/1.1.6.2/KSP-AVC-1.1.6.2.zip";
    sha256 = "132cml88wscrq45glqyvxd1izngkcirnv52nzinaac6ssslc6vk1";
    stripRoot = false;
  };
  gamedataDir = "${mname}/";
}
