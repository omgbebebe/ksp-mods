{ pkgs, stdenv, ksp }:

{ mods ? []
} @ args:

let
  inherit (stdenv.lib) makeOverridable toList;
in
stdenv.mkDerivation {
  name = "ksp-with-modules";
  buildInputs = [ ksp ] ++ mods;
  src = "";
  phases = [ "installPhase" ];
  MODS = toList mods;

  installPhase = ''
    echo "buildInputs: $buildInputs"
    rm -rf $out
    mkdir -p $out
    ln -s ${ksp}/* $out/
    rm $out/GameData
    mkdir $out/GameData
    ln -s ${ksp}/GameData/* $out/GameData/
    echo "MODS: $MODS"
    for m in $MODS; do
      echo "add module: $m"
      ln -s $m/GameData/* $out/GameData/
    done
  '';
}
