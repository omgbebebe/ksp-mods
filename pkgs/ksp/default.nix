{ stdenv, lib, fetchurl, patchelf, writeTextFile
, libX11, libXv, libXext, libXi, libXrender, libXrandr, libXcursor
, gnome2, glib, gdk_pixbuf, libudev0-shim, libpulseaudio
, unzip
}:

{ version
, url
, sha256
, mods ? []
} @args:

stdenv.mkDerivation rec {
  name = "ksp-${version}";
  sourceRoot = "data/noarch/game";

  phases = "unpackPhase installPhase";
  outputs = [ "out" ];

  src = fetchurl {
    inherit url;
    inherit sha256;
  };

  unpackPhase = ''
    unzip -qq ${src} || true
  '';

  installPhase = ''
    mkdir -p $out/bin
    cp -r ./* $out/
    mkdir $out/Plugins

    paxmark m $out/KSP.x86_64
    patchelf --interpreter "$(cat $NIX_CC/nix-support/dynamic-linker)" \
        --set-rpath "${lib.makeLibraryPath buildInputs}" $out/KSP.x86_64

    find $out/ -name '*.so' | xargs -I% \
      patchelf --set-rpath "${lib.makeLibraryPath buildInputs}" %

    #ldd $out/KSP_Data/Plugins/x86_64/ScreenSelector.so
    cat > $out/bin/ksp_x64 << EOF
    #!${stdenv.shell}
    exec $out/KSP.x86_64 -nolog "\$@"
    EOF

    chmod +x $out/bin/ksp_x64
  '';

  nativeBuildInputs = [
    unzip
  ];

  buildInputs = [
    stdenv.glibc
    stdenv.cc.cc
    libX11
    libXv
    libXext
    libXi
    libXrender
    libXrandr
    libXcursor

    # dynamic deps
    gnome2.gtk
    glib
    gdk_pixbuf
    libudev0-shim
    libpulseaudio
  ] ++ mods;

  meta = {
    description = "Kerbal Space Program";
    homepage = "https://kerbalspaceprogram.com/";
    license = stdenv.lib.licenses.unfree;
  };
}
