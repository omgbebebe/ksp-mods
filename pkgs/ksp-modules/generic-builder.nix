{ stdenv, lib, mono, glibcLocales, ksp }:

{ pname
, compiler
, version, revision ? null
, sha256 ? null
, src
, description ? ""
, requiredMods ? []
, suggestedMods ? []
, supportedMods ? []
, withSuggested ? false
, withSupported ? false

, configureFlags ? []
, doCheck ? false
, dontStrip ? true
, hardeningDisable ? true

, homepage ? ""
, platforms ? stdenv.meta.platforms
, hydraPlatforms ? platforms
, license
, maintainers ? []
, broken ? false
, preCompileBuildDriver ? "", postCompileBuildDriver ? ""
, preUnpack ? "", postUnpack ? ""
, patches ? [], patchPhase ? "", prePatch ? "", postPatch ? ""
, preConfigure ? "", postConfigure ? ""
, preBuild ? "", postBuild ? ""
, installPhase ? "", preInstall ? "", postInstall ? ""
, checkPhase ? "", preCheck ? "", postCheck ? ""
, preFixup ? "", postFixup ? ""
, shellHook ? ""
} @ args:

let
  inherit (stdenv.lib) optional optionals optionalAttrs;

  buildInputs = [ ];
  propagatedBuildInputs = requiredMods ++
    optionals withSuggested suggestedMods ++
    optionals withSupported supportedMods;
    
in

stdenv.mkDerivation ({
  name = "${pname}-${version}";
  
  inherit src;

  nativeBuildInputs = [ ksp mono ];

  KSPDIR="${ksp.out}";
  outputs = [ "out" ];
  GAMEDATA_PATH = "${compiler.gameDataPath}";

  buildPhase = ''
    runHook preBuild

    make
    make package

    runHook postBuild
  '';

  installPhase = ''
    runHook preInstall

    mkdir $out
    cp -rv $GAMEDATA_PATH $out

    runHook postInstall
  '';

  meta = { inherit homepage license platforms; }
         // optionalAttrs broken               { inherit broken; }
         // optionalAttrs (description != "")  { inherit description; }
         // optionalAttrs (maintainers != [])  { inherit maintainers; }
         // optionalAttrs (hydraPlatforms != platforms) { inherit hydraPlatforms; }
         ;
}
// optionalAttrs (preCompileBuildDriver != "")  { inherit preCompileBuildDriver; }
// optionalAttrs (postCompileBuildDriver != "") { inherit postCompileBuildDriver; }
// optionalAttrs (preUnpack != "")      { inherit preUnpack; }
// optionalAttrs (postUnpack != "")     { inherit postUnpack; }
// optionalAttrs (configureFlags != []) { inherit configureFlags; }
// optionalAttrs (patches != [])        { inherit patches; }
// optionalAttrs (patchPhase != "")     { inherit patchPhase; }
// optionalAttrs (preConfigure != "")   { inherit preConfigure; }
// optionalAttrs (postConfigure != "")  { inherit postConfigure; }
// optionalAttrs (preBuild != "")       { inherit preBuild; }
// optionalAttrs (postBuild != "")      { inherit postBuild; }
// optionalAttrs (doCheck)              { inherit doCheck; }
// optionalAttrs (checkPhase != "")     { inherit checkPhase; }
// optionalAttrs (preCheck != "")       { inherit preCheck; }
// optionalAttrs (postCheck != "")      { inherit postCheck; }
// optionalAttrs (preInstall != "")     { inherit preInstall; }
// optionalAttrs (installPhase != "")   { inherit installPhase; }
// optionalAttrs (postInstall != "")    { inherit postInstall; }
// optionalAttrs (preFixup != "")       { inherit preFixup; }
// optionalAttrs (postFixup != "")      { inherit postFixup; }
// optionalAttrs (dontStrip)            { inherit dontStrip; }
// optionalAttrs (hardeningDisable != []) { inherit hardeningDisable; }
// optionalAttrs (stdenv.isLinux)       { LOCALE_ARCHIVE = "${glibcLocales}/lib/locale/locale-archive"; }
)
