{ stdenv ? pkgs.stdenv
, pkgs ? import <nixpkgs> {} }:

with pkgs;

let
  ksp122 = stdenv.mkDerivation rec {
    name = "ksp-libs-${version}";
    version = "1.2.2";
    src = fetchurl {
      url = "file:///tmp/ksp-libs-${version}.zip";
      sha256 = "0fi8jklrfq95miw73x7fichr6gnliv9mxb1svzh1s75msjpv5083";
    };
    builder = ./builder.sh;
    buildInputs = [ unzip ];
  };
  ksp130 = stdenv.mkDerivation rec {
    name = "ksp-libs-${version}";
    version = "1.3.0";
    src = fetchurl {
      url = "file:///tmp/ksp-libs-${version}.tar.gz";
#      sha256 = "03ryclcdasncnfhsrajkprn0g89qd8yrpdgc3zwmxg64q4cdr6fh";
      sha256 = "12c2yyxbxpw7hrgr50m9a527qfbr1id8vqp9k6fcz993nxzqfx2n";
    };
    phases = [ "unpackPhase" "installPhase" ];
    unpackPhase = ''
      tar xvf $src
    '';
    installPhase = ''
      mkdir -p $out/KSP_Data/Managed
      ls -la
      cp -av *.dll $out/KSP_Data/Managed/
    '';
  };
in ksp130
