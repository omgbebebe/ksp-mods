#!/usr/bin/env bash
source $stdenv/setup
PATH=$unzip/bin:$PATH

unzip $src
mkdir $out
cp *.dll $out
