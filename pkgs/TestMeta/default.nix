{ pkgs ? import <nixpkgs> {} }:

with pkgs;

stdenv.mkDerivation rec {
  name = "TestMeta-${version}";
  version = "dev";
  src = ./.;
  nativeBuildInputs = [ rsync ];
  buildDepends = with kspMods; [
AGExt
AJE
B9-PWings-Fork
Chatterer
CommunityResourcePack
CommunityTechTree
ConnectedLivingSpace
ContractConfigurator
CustomBarnKit
DeadlyReentry
EditorExtensionsRedux
FerramAerospaceResearch
FilterExtension
Firespitter
FMRS
ForScience
HyperEdit
KerbalAlarmClock
KerbalConstructionTime
KerbalEngineer
KerbalJointReinforcement
#KerbalKonstructs
Kopernicus
MagiCore
MechJeb2
ModularFlightIntegrator
ModuleManager
PersistentRotation
Principia
ProceduralFairings
ProceduralFairings-ForEverything
ProceduralParts
RCSBuildAid
RealChute
RealFuels
RealHeat
RealPlume
RealismOverhaul
RP0
RemoteTech
REPOSoftTechKSPUtils
RetractableLiftingSurface
RW-Saturatable
ScrapYard
SmokeScreen
SolverEngines
StageRecovery
SXTContinued
TacLifeSupport
Taerobee
TestFlight
Toolbar
VenStockRevamp
X-Science

#Scatterer
RealSolarSystem
RSS-Textures

  ];
  phases = [ "installPhase" ];
  installPhase = ''
    echo installing ${name}
    mkdir -p $out/GameData
    touch $out/GameData/.placeholder

    for m in $buildDepends; do
      echo pricessing $m
      rsync --chmod=D770,F660 -rptlog  $m/GameData/* $out/GameData/
    done
  '';
}
