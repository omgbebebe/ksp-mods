{ pkgs, stdenv, mono, rsync }:

{ mname
, version
, src
, pluginsDir ? ""
, gamedataDir ? ""
, packageType ? "bin"
, requiredMods ? []
, recommendedMods ? []
, pluginsDst ? "GameData/${mname}/Plugins"
} @ args:

stdenv.mkDerivation rec {
  inherit (pkgs) ksp;
  name = "${mname}-${version}";

  #inherit (pkgs.lib) toList;
  inherit src;

  buildsInput = [ ksp mono rsync ];
#  depMods = [ pkgs.kspMods.ModuleManager ] ++ requiredMods;

  nativeBuildsInput = [ ] ++ requiredMods;

  DEP_MODS = pkgs.lib.toList requiredMods;

  buildPhase = if packageType == "bin" then ''
    runHook preBuild
    echo "binary package, nothing to build"
    find $src -print
    runHook postBuild
  ''
  else if packageType == "src" then
  ''
    runHook preBuild

    echo "build from sources"

    runHook postBuild
  '' else throw "unsupported packageType: ${packageType}";

  installPhase = ''
    runHook preInstall
    echo "Installing ${name}"

    export PATH="${rsync}/bin:$PATH"

    export GAMEDATA_DIR="${gamedataDir}"
    export PLUGINS_DIR="${pluginsDir}"
    echo "GameData dir: $GAMEDATA_DIR"
    echo "Plugins dir: $PLUGINS_DIR"

    if [[ -n "$GAMEDATA_DIR" ]]; then
      mkdir -p $out/GameData/${mname}
      echo "processing ${name} gamedata"
      rsync -a --exclude=Plugins/ $GAMEDATA_DIR/* $out/GameData/${mname}/
      #rsync -av --force --delete-after --exclude=Plugins/ $GAMEDATA_DIR/* $out/GameData/${mname}/
    fi

    if [[ -n "$PLUGINS_DIR" ]]; then
      mkdir -p $out/${pluginsDst}
      echo "processing ${name} plugins"
      rsync -a $PLUGINS_DIR/*.dll $out/${pluginsDst}/
      #rsync -av --force --delete-after $PLUGINS_DIR/*.dll $out/${pluginsDst}/
    fi

    echo "linking dependent mods";
    for m in $DEP_MODS; do
      echo "processing $m"
      ln -sf $m/GameData/* $out/GameData/
    done

    runHook postInstall
  '';
}
