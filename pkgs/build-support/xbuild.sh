#!/usr/bin/env bash

source $stdenv/setup
runHook preBuild

xbuild /property:ReferencePath=$KSP_DIR/KSP_Data/Managed/ /p:PreBuildEvent= /p:PostBuildEvent=

runHook postBuild
