{ stdenv, git }:
{
  name ? ""
, url ? ""
, owner ? ""
, project ? ""
, branch ? "master"
}:

assert url == "";

let
  fetch = args: let
    url = 
in stdenv.mkDerivation rec {
  name = name
  builder = ./build.sh
  src = fetchGit {
    url = url;
    
