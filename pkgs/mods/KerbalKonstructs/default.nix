{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "KerbalKonstructs-${version}";
    version = "master";
    src = fetchFromGitHub {
      owner = "GER-Space";
      repo = "Kerbal-Konstructs";
      rev = "247cea96ff951c56aa4bd0106fe4d75b52efb63a";
      sha256 = "1mn0vdg4az029wz7ihl1ipshgr1bsmm6fqi6ap3smkn8ipzkpzkp";
    };
    nativeBuildInputs = [ git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
#    patches = [ ./build-1.3.0.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= KerbalKonstructs.sln
      comm -12 <(ls -1 GameData/KerbalKonstructs/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f GameData/KerbalKonstructs/%
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData
      cp -av GameData/KerbalKonstructs $out/GameData/
    '';
  };
in src
