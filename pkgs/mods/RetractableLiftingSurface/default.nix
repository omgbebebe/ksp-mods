{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "RetractableLiftingSurface-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "linuxgurugamer";
      repo = "RetractableLiftingSurface";
      rev = "ac90496ff382a68c3123828e6e651554004470e9";
      sha256 = "0wkyd410pg7vw44ndkgv0pwgmh61g7i3kfsj5q839lwnhzjxyqpl";
    };
    nativeBuildInputs = [  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      rm -f GameData/RetractableLiftingSurface/Plugins/*
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= RetractableLiftingSurface.sln
      comm -12 <(ls -1 ./RetractableLiftingSurface/bin/Release/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f ./RetractableLiftingSurface/bin/Release/%
      cp -av RetractableLiftingSurface/bin/Release/* ./GameData/RetractableLiftingSurface/Plugins/
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/
      cp -av ./GameData/RetractableLiftingSurface $out/GameData/
    '';
  };
in src
