{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "RSSVE-${version}";
    version = "KSP-1.3-Update";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "PhineasFreak";
      repo = "RSSVE";
      rev = "390da94fb610b38f2eaa7c817c1eacccdd7eecf3";
      sha256 = "0a3dj4k87fqy9w5qpj7m767gx95iy3asv6fgbv58q3132d56m4db";
    };
    nativeBuildInputs = [ wget git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
#    patches = [ ./ksp-1.3.0.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= Source/RSSVE.sln
      comm -12 <(ls -1 ./RSSVE/Plugins/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f ./RSSVE/Plugins/%
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/
      cp -av ./RSSVE $out/GameData/
      wget --no-check-certificate https://raw.githubusercontent.com/PhineasFreak/DocBin/master/DocBin/generic_files/RSSVE_Scatterer_PlanetFix_Config.cfg -O $out/GameData/RSSVE/RSSVE_Scatterer_PlanetFix_Config.cfg
    '';
  };
in src
