{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "X-Science-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "thewebbooth";
      repo = "KSP-X-Science";
      rev = "e3b08274468d445bdaa2803b083d341cb5c3969f";
      sha256 = "0rkjjji16p4sahrbljn0hfrfm6m1imww5hmknyg4nns4dbrvzsab";
    };
    nativeBuildInputs = [ perlPackages.YAMLLibYAML  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    patches = [ ./fix.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= X-Science/\[x\]\ Science\!.sln
      cp -av X-Science/bin/Release/\[x\]\ Science\!.dll ./X-Science/GameData/\[x\]\ Science\!/
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/
      cp -av ././X-Science/GameData/\[x\]\ Science\! $out/GameData/
    '';
  };
in src
