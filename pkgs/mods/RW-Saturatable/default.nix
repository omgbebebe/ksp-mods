{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "RW-Saturatable-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "Crzyrndm";
      repo = "RW-Saturatable";
      rev = "600eb8cbf38691b96d6f4b89997ebd2e82027bdb";
      sha256 = "1wfyysjb5ba64j12pnsa89q5r9pkx834lh1rz19r2z8wmd3zzmx3";
    };
    nativeBuildInputs = [  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 kspMods.ModularFlightIntegrator ];
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
#    patches = [ ./ksp-1.3.0.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      rm -f ./GameData/"RW Saturatable"/*.dll
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= SaturatableRW.sln
      comm -12 <(ls -1 ./GameData/"RW Saturatable"/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f ./"GameData/RW Saturatable/"%
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/
      cp -av "./GameData/RW Saturatable/" $out/GameData/RWSaturatable
    '';
  };
in src
