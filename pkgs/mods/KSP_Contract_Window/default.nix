{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "KSP_Contract_Window-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "DMagic1";
      repo = "KSP_Contract_Window";
      rev = "8908dec5e91190a0431290a1a3a31f128978018a";
      sha256 = "0qnzr0hbkyclr6qr8xdgm5m0l45gdc3qpz749sb58xwjqyy9fswn";
    };
    nativeBuildInputs = [ git mono50 curl unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= Source/KSPP_Contracts.sln
      cp -av Source/obj/Release/ContractsWindow.Unity.dll ./GameData/DMagicUtilities/ContractsWindow/
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/
      cp -av ./GameData/DMagicUtilities/ContractsWindow $out/GameData/
    '';
  };
in src
