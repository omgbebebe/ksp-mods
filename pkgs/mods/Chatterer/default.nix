{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "Chatterer-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "Athlonic";
      repo = "Chatterer";
      rev = "9050b0b556688b21512532f4de5613eed00998aa";
      sha256 = "0xppzyw7i1pvh0pwnr21prapgw2yfqp5mk1vimp4v393lvgq8jv2";
    };
    nativeBuildInputs = [ git mono50 ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    patches = [ ./fix.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= Chatterer.sln
      comm -12 <(ls -1 ./GameData/Chatterer/Plugins/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f ./GameData/Chatterer/Plugins/%
      cp -av Chatterer/GameData/Chatterer/Textures ./GameData/Chatterer/
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/
      cp -av ./GameData/Chatterer $out/GameData/
      cp -av ${kspMods.ChattererResources}/GameData/Chatterer $out/GameData/
    '';
  };
in src
