{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "ChattererResources-${version}";
    version = "master";
    src = fetchurl {
      url = "https://kerbal.curseforge.com/projects/chatterer/files/2425554/download";
      sha256 = "1h8yckg1zpwim97jcnnnizw49as8bziisw4c9ycd6qvq17p6q996";
    };
    nativeBuildInputs = [ unzip ];
    phases = [ "unpackPhase" "installPhase" ];
    unpackPhase = ''
      unzip $src
      find ./ -iname '*.dll' -delete
    '';
    installPhase = ''
      mkdir -p $out/GameData/
      cp -av ./GameData/Chatterer $out/GameData/
    '';
  };
in src
