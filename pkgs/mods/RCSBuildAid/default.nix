{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "RCSBuildAid-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "m4v";
      repo = "RCSBuildAid";
      rev = "a80e5493a31eb657205e0a2816efe8ca8381c97f";
      sha256 = "0d4kwsc040gvyjsqgk3zlysx54shjimaww7s79z6yi92390a398l";
    };
    nativeBuildInputs = [  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ ( with kspMods; [
      Toolbar
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    patches = [ ./ksp-1.3.0.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    TB = "${kspMods.Toolbar.out}/GameData/000_Toolbar";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}%3b${TB}" /p:PreBuildEvent= /p:PostBuildEvent= ./RCSBuildAid.sln
      comm -12 <(ls -1 ././bin/Release/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f ././bin/Release/%
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/RCSBuildAid/Plugins
      cp -av ./bin/Release/* $out/GameData/RCSBuildAid/Plugins/
      cp -av ./Textures $out/GameData/RCSBuildAid/
    '';
  };
in src
