{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "ProceduralFairings-ForEverything-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "KSP-RO";
      repo = "ProceduralFairings-ForEverything";
      rev = "912fe96bf400382779e8c10fca1e89fcc94246b2";
      sha256 = "13f66kripbl0r3ai8m2whwwkj3jlcwalfn0jp21nbsyvci1jkz0i";
    };
    nativeBuildInputs = [  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 kspMods.Firespitter ];
    phases = [ "unpackPhase" "installPhase" ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    installPhase = ''
      mkdir -p $out/GameData
      cp -av GameData/ProceduralFairings-ForEverything $out/GameData/
    '';
  };
in src
