{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "REPOSoftTechKSPUtils-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "JPLRepo";
      repo = "REPOSoftTechKSPUtils";
      rev = "7c391f519f2e01e7ed6f89a71b223dcfc251b8eb";
      sha256 = "17wjl4dj3211qmpqcrph2lxviyiamamw69nvxd273jv2x6hj9r72";
    };
    nativeBuildInputs = [  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ];
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
#    patches = [ ./ksp-1.3.0.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      mkdir libs
      xbuild /p:Configuration=Release /p:out= /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= BackgroundResources/BackgroundResources.csproj
      mv ./BackgroundResources/bin/Release/* ./libs/

      xbuild /p:Configuration=Release /p:out= /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= RSTKSPGameEvents/RSTKSPGameEvents.csproj
      mv ./RSTKSPGameEvents/bin/Release/* ./libs/

      xbuild /p:Configuration=Release /p:out= /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= StringFormatter/StringFormatter.csproj
      mv ./StringFormatter/bin/Release/* ./libs/

      comm -12 <(ls -1 ./libs/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f ./libs/%
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/REPOSoftTechKSPUtils
      cp -av ./* $out/
      cp -av libs/* $out/GameData/REPOSoftTechKSPUtils/
    '';
  };
in src
