{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "kopernicus-${version}";
    version = "master";
    src = pkgs.fetchFromGitHub {
      fetchSubmodules = true;
      owner = "Kopernicus";
      repo = "Kopernicus";
#      rev = "e23de9f7075a8849fddf9717c5f6ce27e2216f64";
#      sha256 = "0n6rw64yf0yixxrxshp9qizk6hn12kb1dv1syzxa4g7xxnidzinm";
      rev = "6d46eac275830c45bdbea89e3ebf141ff7128a5a";
      sha256 = "0v76p1jahrpqwrbd12mnilzgcsgfh1qq6rs6asd5pxjqfr5rmvrf";
    };
    nativeBuildInputs = [ git mono50 cacert which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 kspMods.ModularFlightIntegrator ];
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    patches = [ ./nix-build.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    MFI = "${kspMods.ModularFlightIntegrator}/GameData/ModularFlightIntegrator";
    buildPhase = ''
      runHook preBuild
      make release
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData
      cp -av Distribution/Release/GameData/Kopernicus $out/GameData/
    '';
  };
in src
