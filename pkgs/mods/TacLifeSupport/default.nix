{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "TacLifeSupport-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "KSP-RO";
      repo = "TacLifeSupport";
      rev = "735434fcacbc0461917d1b415d1ea9180c13ace2";
      sha256 = "0vqjywar68gvrqvmbrsf6lhz4xhw1d4id4g534ndbv45bnwck8h2";
    };
    nativeBuildInputs = [  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 kspMods.REPOSoftTechKSPUtils ];
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    patches = [ ./fix.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    RSTU = "${kspMods.REPOSoftTechKSPUtils.out}";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}%3b${RSTU}/libs/" /p:PreBuildEvent= /p:PostBuildEvent= Source/TacLifeSupport.sln
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/
      cp -av ./Release/_/GameData/* $out/GameData/
      mv $out/GameData/ThunderAerospace/Plugins $out/GameData/ThunderAerospace/TacLifeSupport/
      rm -f GameData/ThunderAerospace/MiniAVC.dll
    '';
  };
in src
