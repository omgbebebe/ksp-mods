{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "PersistentRotation-${version}";
    version = "Release";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "MarkusA380";
      repo = "PersistentRotation";
      rev = "4388c313bc92fd495f22028e45f436235ca400e8";
      sha256 = "14pq1qd076v49j3dh29l8nivl59l1g4pw363rnvpqxhi18mzs1mc";
    };
    nativeBuildInputs = [ git mono50 ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
#    patches = [ ./ksp-1.3.0.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      rm -f GameData/PersistentRotation/Plugins/PersistentRotation.dll
      pushd Source
      csc /noconfig /target:library /langversion:6 /nostdlib+ /checked- /platform:x64 /optimize+ /reference:"${KSP_LIBS}/Assembly-CSharp.dll" /reference:"${KSP_LIBS}/mscorlib.dll" /reference:"${KSP_LIBS}/System.Core.dll" /reference:"${KSP_LIBS}/System.dll" /reference:"${KSP_LIBS}/UnityEngine.dll" /reference:"${KSP_LIBS}/UnityEngine.UI.dll" /out:../GameData/PersistentRotation/Plugins/PersistentRotation.dll /recurse:'*.cs'
      popd
      comm -12 <(ls -1 ./GameData/PersistentRotation/Plugins/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f ./GameData/PersistentRotation/Plugins/%
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/
      cp -av ./GameData/PersistentRotation $out/GameData/
    '';
  };
in src
