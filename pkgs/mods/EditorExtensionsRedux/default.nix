{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "EditorExtensionsRedux-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "linuxgurugamer";
      repo = "EditorExtensionsRedux";
      rev = "43bff2940a065a103081e6514dce39afcb4a6277";
      sha256 = "1fbzapv3k9r8r31dvlvh36n0lg9lk6sxj0iqid1v9ilaqi0zwqlg";
    };
    nativeBuildInputs = [ git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
#    patches = [ ./ksp-1.3.0.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= EditorExtensionsRedux.sln
      cp -av EditorExtensionsRedux/bin/Release/EditorExtensionsRedux.dll GameData/EditorExtensionsRedux/
#      comm -12 <(ls -1 ./Kerbal_Construction_Time/bin/Release/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f ./Kerbal_Construction_Time/bin/Release/%
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/
      cp -av ./GameData/EditorExtensionsRedux $out/GameData/
    '';
  };
in src
