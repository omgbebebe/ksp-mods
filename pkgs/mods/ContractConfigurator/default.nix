{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "contractconfigurator-${version}";
    version = "master";
    src = fetchFromGitHub {
      owner = "jrossignol";
      repo = "ContractConfigurator";
      rev = "e20a639b7a52225717064ff7e46eefb6495b9d5a";
      sha256 = "1ixmkk0mx4qkricxla6v8lqf68nd4j6gmmks126f7307mlm5dicw";
    };
    nativeBuildInputs = [ git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
      RemoteTech
      KerbalKonstructs
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    patches = [ ./filename-fix.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    RT = "${kspMods.RemoteTech.out}/GameData/RemoteTech/Plugins";
    KK = "${kspMods.KerbalKonstructs.out}/GameData/KerbalKonstructs";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}%3b${RT}%3b${KK}" /p:PreBuildEvent= /p:PostBuildEvent= source/ContractConfigurator.sln
      comm -12 <(ls -1 GameData/ContractConfigurator/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f GameData/ContractConfigurator/%
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData
      cp -av GameData/ContractConfigurator $out/GameData/
    '';
  };
in src
