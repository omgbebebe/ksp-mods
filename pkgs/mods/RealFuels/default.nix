{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "realfuels-${version}";
    version = "master";
    src = fetchFromGitHub {
      owner = "NathanKell";
      repo = "ModularFuelSystem";
      rev = "d3d755c5b440a95eb621d25a15f48a0d77790ba3";
      sha256 = "1g2mxb6vbd2dnzznljmq6mfpbhx5lwg9vdv73i8xswp2947c6zvn";
    };
    nativeBuildInputs = [ git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
      SolverEngines
      TestFlight
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    patches = [ ./fix.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    SE = "${kspMods.SolverEngines}/GameData/SolverEngines/Plugins";
    TF = "${kspMods.TestFlight}/GameData/TestFlight/Plugins";
    buildPhase = ''
      runHook preBuild
      substitute Source/assembly/AssemblyInfoRF.in Source/assembly/AssemblyInfoRF.cs \
        --replace '@VERSION@' '1' \
        --replace '@FULL_VERSION@' '1' \
        --replace '@FILE_VERSION@' '1'
      cat Source/assembly/AssemblyInfoRF.cs
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}%3b${SE}%3b${TF}" /p:PreBuildEvent= /p:PostBuildEvent= Source/RealFuels.sln
      comm -12 <(ls -1 RealFuels/Plugins/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f RealFuels/Plugins/%
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData
      cp -av RealFuels $out/GameData/
    '';
  };
in src
