{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "AGExtData-${version}";
    version = "2.2";
    src = fetchurl {
      url = "https://github.com/SirDiazo/AGExt/releases/download/2.2/ActionGroupsExtended22.zip";
      sha256 = "07ghv00zn4qzqrk0mvqw3q3xiakwz76rb3ljj9ddjvy1ivd2rzr5";
    };
    nativeBuildInputs = [ unzip ];
    phases = [ "unpackPhase" "installPhase" ];
    unpackPhase = ''
      unzip $src
      rm -f GameData/Diazo/AGExt/*.dll
    '';
    installPhase = ''
      mkdir -p $out/GameData/Diazo/AGExt
      cp -av ./GameData/Diazo/AGExt $out/GameData/Diazo/
    '';
  };
in src
