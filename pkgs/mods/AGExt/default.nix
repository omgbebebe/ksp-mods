{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "AGExt-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "SirDiazo";
      repo = "AGExt";
      rev = "43c836ad202cf91cf960efd5bdf1d2ee0b812fda";
      sha256 = "0ck9yjm4cmb4sb1r5brqgbags3jyq2vyj38ghnhkmmf6hfdnqsnd";
    };
    nativeBuildInputs = [ git mono50 curl unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= AGExt.sln
      
      mkdir -p ./GameData/AGExt
      cp -av ./AGExt/obj/Release/AGExt.dll ./GameData/AGExt/
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/Diazo
      cp -av ./GameData/AGExt $out/GameData/Diazo/
      cp -av ${kspMods.AGExtData}/GameData/Diazo/AGExt/* ./GameData/AGExt $out/GameData/Diazo/AGExt/
    '';
  };
in src
