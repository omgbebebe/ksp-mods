{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "Taerobee-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "Tantares";
      repo = "Taerobee";
      rev = "7091ecf4c1869fdcc0d99747a64f7b7ff5bdabda";
      sha256 = "1dqkm0h0lqpcqk2pjb0wmri51dpnh101l0hvi8jja8qx6lpc8cpf";
    };
    nativeBuildInputs = [  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "installPhase" ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    installPhase = ''
      mkdir -p $out/GameData/
      cp -av ./GameData/Taerobee $out/GameData/
    '';
  };
in src
