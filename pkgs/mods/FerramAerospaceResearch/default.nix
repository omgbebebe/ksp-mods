{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  mname = "FerramAerospaceResearch";
  requiredMods = with pkgs.kspMods; [ ModularFlightIntegrator ];

  src = stdenv.mkDerivation rec {
    name = "ferramaerospaceresearch-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "ferram4";
      repo = "Ferram-Aerospace-Research";
      rev = "d82a3a4282e72a9488ba98045c59b7d2e56fc3d5";
      sha256 = "072wnnrczg0r6cizpcwiy3616773p5k91ylgc90bbypvzvzkg0z5";
    };
    nativeBuildInputs = [ git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 kspMods.ModularFlightIntegrator ];
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    patches = [ ./ksp-1.3.0.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    MFI = "${kspMods.ModularFlightIntegrator}/GameData/ModularFlightIntegrator";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}%3b${MFI}" FerramAerospaceResearch.sln
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData
      cp -av GameData/FerramAerospaceResearch $out/GameData/
    '';
  };
in src
