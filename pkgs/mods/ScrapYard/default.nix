{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "ScrapYard-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "magico13";
      repo = "ScrapYard";
      rev = "ca32f6cf5583590efd56cfbed7fbf5158b3c1dd5";
      sha256 = "02cnrfi8wrqacj7nbp8cii66q16c3sg1p4r0f0shi8ib53g8wjgw";
    };
    nativeBuildInputs = [ git mono50 curl unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
      MagiCore
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}%3b${kspMods.MagiCore}/GameData/MagiCore" /p:PreBuildEvent= /p:PostBuildEvent= ScrapYard.sln 
      cp -av ScrapYard/bin/Release/ScrapYard.dll GameData/ScrapYard/
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData
      cp -av ./GameData/ScrapYard $out/GameData/
    '';
  };
in src
