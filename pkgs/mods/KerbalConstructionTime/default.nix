{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "KerbalConstructionTime-${version}";
    version = "development";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "magico13";
      repo = "KCT";
      rev = "69abe4d54efcaa869927628cbf22d2b35eb8be75";
      sha256 = "00cxv9wp64p7k3yac5iyjpx159iw23438djj65f37fzg6w4jwxl8";
    };
    nativeBuildInputs = [  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
      MagiCore
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
#    patches = [ ./ksp-1.3.0.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    MC = "${kspMods.MagiCore.out}/GameData/MagiCore";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}%3b${MC}" /p:PreBuildEvent= /p:PostBuildEvent= KerbalConstructionTime.sln
      comm -12 <(ls -1 ./Kerbal_Construction_Time/bin/Release/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f ./Kerbal_Construction_Time/bin/Release/%
      mkdir GameData/KerbalConstructionTime/Plugins
      cp -av Kerbal_Construction_Time/bin/Release/KerbalConstructionTime.* ./GameData/KerbalConstructionTime/Plugins/
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/
      cp -av ./GameData/KerbalConstructionTime $out/GameData/
    '';
  };
in src
