{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "FilterExtension-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "Crzyrndm";
      repo = "FilterExtension";
      rev = "6926712b4309bd0852663806244aa4169c1d429e";
      sha256 = "0vn5hs49wwazvi8p6pfl5ghl6m91nr32xw6x09g1x8qmc8awba48";
    };
    nativeBuildInputs = [  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ];
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
#    patches = [ ./csproj.patch ./final.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      rm -rf DeadlyReentry/Plugins/*
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= FilterExtension.sln
      comm -12 <(ls -1 GameData/000_FilterExtensions/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f GameData/000_FilterExtensions/%
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData
      cp -av GameData/000_FilterExtensions $out/GameData/
      cp -av GameData/"000_FilterExtensions Configs" $out/GameData/000_FilterExtensionsConfigs
    '';
  };
in src
