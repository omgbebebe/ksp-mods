{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "KerbalJointReinforcement-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "ferram4";
      repo = "Kerbal-Joint-Reinforcement";
      rev = "6c0614b1b66b3e63e9aa1e0c1cd1ef2fa044eb72";
      sha256 = "06mcs23jm3wrdq3jqijr8jh1h1qpwwkxnp415km9wjnykk96sznk";
    };
    nativeBuildInputs = [ git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ];
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    patches = [ ./ksp-1.3.0.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= KerbalJointReinforcement/KerbalJointReinforcement.sln
      cp -av KerbalJointReinforcement/KerbalJointReinforcement/bin/Release/KerbalJointReinforcement.dll GameData/KerbalJointReinforcement/Plugin/
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData
      cp -av GameData/KerbalJointReinforcement/Plugin $out/GameData/KerbalJointReinforcement
    '';
  };
in src
