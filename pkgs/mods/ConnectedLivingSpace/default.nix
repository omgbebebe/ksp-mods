{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "ConnectedLivingSpace-${version}";
    version = "master";
    src = fetchFromGitHub {
      owner = "codepoetpbowden";
      repo = "ConnectedLivingSpace";
      rev = "c65e6f070ded4e9bb5c65114b3f75b61b4ec36a2";
      sha256 = "1aillka7gky8pskpp9mrpcjl9z0j508yqqbv1fa4p9hzyb56ifq7";
    };
    nativeBuildInputs = [ git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
      ModularFlightIntegrator
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    patches = [ ./projects.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    MFI = "${kspMods.ModularFlightIntegrator}/GameData/ModularFlightIntegrator";
    buildPhase = ''
      runHook preBuild
      rm -f ./plugins/ConnectedLivingSpace/Distribution/GameData/ConnectedLivingSpace/Plugins/*.dll
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}%3b${MFI}" /p:PreBuildEvent= /p:PostBuildEvent= plugins/ConnectedLivingSpace/ConnectedLivingSpace.sln
      cp plugins/ConnectedLivingSpace/bin/Release/CLSInterfaces.* ./plugins/ConnectedLivingSpace/Distribution/GameData/ConnectedLivingSpace/Plugins/
      cp plugins/ConnectedLivingSpace/bin/Release/ConnectedLivingSpace.* ./plugins/ConnectedLivingSpace/Distribution/GameData/ConnectedLivingSpace/Plugins/
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/ConnectedLivingSpace
      cp -av ./plugins/ConnectedLivingSpace/Distribution/GameData/ConnectedLivingSpace $out/GameData/
    '';
  };
in src
