{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "RSS-Textures-${version}";
    version = "10.4";
    src = fetchurl {
      url = "https://github.com/KSP-RO/RSS-Textures/releases/download/v10.4/8192.zip";
      sha256 = "1n1pw4y6l16j8vg98d07rw539rqsy1b5nv426pm6v9q9zpssfcn8";
    };
    nativeBuildInputs = [ unzip ];
    phases = [ "unpackPhase" "installPhase" ];
    unpackPhase = ''
      unzip $src
    '';
    installPhase = ''
      mkdir -p $out/GameData
      cp -av ./GameData/RSS-Textures $out/GameData/
    '';
  };
in src
