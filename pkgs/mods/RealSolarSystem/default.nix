{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "realsolarsystem-${version}";
    version = "master";
    src = fetchFromGitHub {
      owner = "KSP-RO";
      repo = "RealSolarSystem";
      rev = "ea181fbbf9bfb411ed5cfe4369c69505e902aea1";
      sha256 = "1hx2vdni2m6g4lirr2r0908jq08dbdcxhx6aswpc23n8fjh6palg";
    };
    nativeBuildInputs = [  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 kspMods.ModularFlightIntegrator kspMods.RSS-Textures ];
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    patches = [ ./ksp-1.3.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      pushd Source
      xbuild /p:Configuration=Release /p:target=x64 /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= RealSolarSystem.sln
      popd
      rm -f GameData/RealSolarSystem/Plugins/System.Core.dll
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData
      cp -av GameData/RealSolarSystem $out/GameData/
      cp -av ${kspMods.RSS-Textures}/GameData/RSS-Textures $out/GameData/
    '';
  };
in src
