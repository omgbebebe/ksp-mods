{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "StageRecovery-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "magico13";
      repo = "StageRecovery";
      rev = "825ae315661b77c957c44db123aa54589941bc49";
      sha256 = "02mxlxdh77cbnj5wj01psi7nix8n2zaqj4n2znkg4cyhnl1slddy";
    };
    nativeBuildInputs = [ git mono50 ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
#    patches = [ ./ksp-1.3.0.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= StageRecovery.sln
      cp -av StageRecovery/bin/Release/StageRecovery.dll GameData/StageRecovery/
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/
      cp -av ./GameData/StageRecovery $out/GameData/
    '';
  };
in src
