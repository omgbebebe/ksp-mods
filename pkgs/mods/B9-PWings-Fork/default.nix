{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "B9-PWings-Fork-${version}";
    version = "master";
    src = fetchFromGitHub {
      owner = "Crzyrndm";
      repo = "B9-PWings-Fork";
      rev = "8b6827b3e35885c151cc235463270e84cfd7bc09";
      sha256 = "0lnbz3g3fpx7lfynksxcw47jwvkap33mnzidikwx6xnp416z03lq";
    };
    nativeBuildInputs = [ git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
      ModularFlightIntegrator
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
#    patches = [ ./fix.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    MFI = "${kspMods.ModularFlightIntegrator}/GameData/ModularFlightIntegrator";
    buildPhase = ''
      runHook preBuild
      rm -f GameData/B9_Aerospace_ProceduralWings/Plugins/*.dll
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}%3b${MFI}" /p:PreBuildEvent= /p:PostBuildEvent= "B9 PWings Fork.sln"
      comm -12 <(ls -1 GameData/B9_Aerospace_ProceduralWings/Plugins/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f GameData/B9_Aerospace_ProceduralWings/Plugins/%
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData
      cp -av GameData/B9_Aerospace_ProceduralWings $out/GameData/
    '';
  };
in src
