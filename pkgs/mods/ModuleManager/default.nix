{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "ModuleManager-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "sarbian";
      repo = "ModuleManager";
      rev = "ceabfeb24a734382ef08eb4af5299b6bc99dbb14";
      sha256 = "0sgydcg7dr09jfir028j44kdk3fpx06alvawvd47abmpvmzwqn1m";
    };
    nativeBuildInputs = [ git mono50 curl unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= ModuleManager.sln
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/
      cp -av ./bin/Release/ModuleManager.dll $out/GameData/
    '';
  };
in src
