{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "HyperEdit-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "Ezriilc";
      repo = "HyperEdit";
      rev = "5f1a345d9d59aa76ee64aecb17046974110b0978";
      sha256 = "0zmnyvfr90ljfnvrd0rpv2wq14rslsl853imqkkd3k9piv52cz13";
    };
    nativeBuildInputs = [ git mono50 ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    patches = [ ./fix.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= HyperEdit.csproj
      
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/HyperEdit
      cp -av ./bin/Release/HyperEdit.dll $out/GameData/HyperEdit/
    '';
  };
in src
