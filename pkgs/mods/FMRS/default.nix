{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "FMRS-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "linuxgurugamer";
      repo = "FMRS";
      rev = "d7830979d508cd888702c5c5158422b7f5eb1fd1";
      sha256 = "0wwnd7f6ln4nymbl91g9dlfdv764xci9m4k262i9ckvai18lwg2c";
    };
    nativeBuildInputs = [ git mono50 ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    patches = [ ./fix_crf.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= Source/FMRS.sln
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= RecoveryController/RecoveryController.csproj
      
      cp -av ./Source/bin/Release/FMRS.dll ./GameData/FMRS/
      cp -av ./RecoveryController/bin/Release/RecoveryController.dll ./GameData/RecoveryController
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/
      cp -av ./GameData/FMRS $out/GameData/
      cp -av ./GameData/RecoveryController $out/GameData/
    '';
  };
in src
