{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "SXTContinued-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "linuxgurugamer";
      repo = "SXTContinued";
      rev = "62ea692626dcdf8b63b2f9bc7e1dd5bd4ee87f2a";
      sha256 = "0zlzxgh4g21q9vfc0dh4dk0jp75hlmv7jl2hx9dr3svy1dqnp1am";
    };
    nativeBuildInputs = [  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
#    patches = [ ./ksp-1.3.0.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      mv GameData/SXT/PlugIns GameData/SXT/Plugins
      rm -f GameData/SXT/Plugins/*
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= ModuleBounce/ModuleBounce.sln
      comm -12 <(ls -1 ./ModuleBounce/bin/Release/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f ./ModuleBounce/bin/Release/%
      cp -av ModuleBounce/bin/Release/* GameData/SXT/Plugins/
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/
      cp -av ./GameData/SXT $out/GameData/
    '';
  };
in src
