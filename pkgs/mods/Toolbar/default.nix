{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "Toolbar-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "blizzy78";
      repo = "ksp_toolbar";
      rev = "141485a715d11ac3d15d8d6f0ffafe5a01d853ba";
      sha256 = "143fnak7r0h0324s5g04cash4fl1kmix999scjxrwagld0aji36q";
    };
    nativeBuildInputs = [  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 kspMods.ModularFlightIntegrator ];
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    patches = [ ./ksp-1.3.0.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= ./Toolbar.sln
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/000_Toolbar
      cp -av ./Toolbar/etc/*.png $out/GameData/000_Toolbar/
      cp -av ./Toolbar/obj/Release/aaa_Toolbar.dll $out/GameData/000_Toolbar/
    '';
  };
in src
