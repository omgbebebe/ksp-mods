{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "ForScience-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "WaveFunctionP";
      repo = "ForScience";
      rev = "cbbec214066e6737ae47a4be8866b52a5ab6feb4";
      sha256 = "1qglrpyhlnmrmgm9l838d2x0yhqrhl14bc3yrkc2piqz4fgssd91";
    };
    nativeBuildInputs = [  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
#    patches = [ ./ksp-1.3.0.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      rm -f Source/GameData/ForScience/Plugins/*
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= Source/ForScience.sln
      comm -12 <(ls -1 ./Source/GameData/ForScience/Plugins/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f ./Source/GameData/ForScience/Plugins/%
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/
      cp -av ./Source/GameData/ForScience $out/GameData/
    '';
  };
in src
