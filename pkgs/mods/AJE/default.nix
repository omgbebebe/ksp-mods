{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "aje-${version}";
    version = "master";
    src = fetchFromGitHub {
      owner = "KSP-RO";
      repo = "AJE";
      rev = "b64cd383ca1208ce4fcb78cf5394a57040b4ba03";
      sha256 = "00yjkasmrqypkhg75f0qy68da7s2z25ili92iqr5g0zaainkb2z7";
    };
    nativeBuildInputs = [ git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 kspMods.SolverEngines ];
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
#    patches = [ ./build-1.3.0.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    SE = "${kspMods.SolverEngines}/GameData/SolverEngines/Plugins";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}%3b${SE}" /p:PreBuildEvent= /p:PostBuildEvent= Source/AJE.sln
      comm -12 <(ls -1 GameData/AJE/Plugins/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f GameData/AJE/Plugins/%
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData
      cp -av GameData/AJE $out/GameData/
    '';
  };
in src
