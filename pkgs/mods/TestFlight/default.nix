{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "testflight-${version}";
    version = "master";
    src = fetchFromGitHub {
      owner = "KSP-RO";
      repo = "TestFlight";
      rev = "14c512914cb01a188d4029768d8773f776ab99ed";
      sha256 = "0r0rqbf6bjm6ckf4drv0va09ghsyvh337yd7z1ch17c5r1wx6skv";
    };
    nativeBuildInputs = [ git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
      ContractConfigurator
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    patches = [ ./fix.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    SE = "${kspMods.SolverEngines}/GameData/SolverEngines/Plugins";
    CC = "${kspMods.ContractConfigurator}/GameData/ContractConfigurator";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}%3b${SE}%3b${CC}" /p:PreBuildEvent= /p:PostBuildEvent= TestFlight.sln
      comm -12 <(ls -1 GameData/TestFlight/Plugins/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f GameData/TestFlight/Plugins/%
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData
      cp -av GameData/TestFlight $out/GameData/
    '';
  };
in src
