{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "ProceduralFairings-${version}";
    version = "master";
    src = fetchFromGitHub {
      owner = "e-dog";
      repo = "ProceduralFairings";
      rev = "01ad72c13b45ecf6a631eb9ba12de0b76108e092";
      sha256 = "1bal25wy2ka2l82k3n1cwmk26d2zc4k8qk8rws942c8l88w9n74z";
    };
    nativeBuildInputs = [  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ];
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      rm -f GameData/ProceduralFairings/*.dll
      pushd Source
      csc /noconfig /target:library /langversion:6 /nostdlib+ /checked- /platform:x64 /optimize+ /out:../GameData/ProceduralFairings/ProceduralFairings.dll /r:${KSP_LIBS}/Assembly-CSharp.dll /r:${KSP_LIBS}/UnityEngine.dll /r:${KSP_LIBS}/UnityEngine.UI.dll /reference:"${KSP_LIBS}/mscorlib.dll" /reference:"${KSP_LIBS}/System.Core.dll" /reference:"${KSP_LIBS}/System.dll" /recurse:'*.cs'
      popd
      ls -la GameData/ProceduralFairings
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData
      cp -av GameData/ProceduralFairings $out/GameData/
    '';
  };
in src
