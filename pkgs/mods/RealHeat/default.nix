{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "RealHeat-${version}";
    version = "master";
    src = fetchFromGitHub {
      owner = "KSP-RO";
      repo = "RealHeat";
      rev = "5d5c04593856588ca34cf38672952901a9e38293";
      sha256 = "1n74sx62kik4yy1yqlqwlry8bwfy0hn6xvbg1400nfid2rfinryi";
    };
    nativeBuildInputs = [ git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
      ModularFlightIntegrator
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    patches = [ ./ksp-1.3.0.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    MFI = "${kspMods.ModularFlightIntegrator}/GameData/ModularFlightIntegrator";
    buildPhase = ''
      runHook preBuild
      rm -f RealHeat/Plugins/*
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}%3b${MFI}" /p:PreBuildEvent= /p:PostBuildEvent= Source/RealHeat.sln
      comm -12 <(ls -1 RealHeat/Plugins/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f RealHeat/Plugins/%
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData
      cp -av RealHeat $out/GameData/
    '';
  };
in src
