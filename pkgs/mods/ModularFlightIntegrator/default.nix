{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

stdenv.mkDerivation rec {
  name = "ModularFlightIntegrator-${version}";
  version = "master";
  src = fetchFromGitHub {
    owner = "sarbian";
    repo = "ModularFlightIntegrator";
    rev = "448a324efa30c4f47f9198c68982e0df5ada5bc9";
    sha256 = "1rmq2bc5wbj42gqwx06622fk369h81jdpqrif5da1k1iwr4rni9x";
  };

  nativeBuildInputs = [ mono50 ];
  buildInputs = [ ksp130 ];
  phases = [ "unpackPhase" "buildPhase" "installPhase" ];
  KSP_DIR = "${ksp130}";
  KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";

  buildPhase = ''
    runHook preBuild
    csc /noconfig /target:library /langversion:6 /nostdlib+ /checked- /platform:x64 /optimize+ /reference:"${KSP_LIBS}/Assembly-CSharp.dll" /reference:"${KSP_LIBS}/mscorlib.dll" /reference:"${KSP_LIBS}/System.Core.dll" /reference:"${KSP_LIBS}/System.dll" /reference:"${KSP_LIBS}/UnityEngine.dll" /reference:"${KSP_LIBS}/UnityEngine.UI.dll" /out:ModularFlightIntegrator/ModularFlightIntegrator.dll /recurse:'*.cs'
    runHook postBuild
  '';

  installPhase = ''
    mkdir -p $out/GameData/ModularFlightIntegrator
    cp -av ModularFlightIntegrator/ModularFlightIntegrator.dll $out/GameData/ModularFlightIntegrator
  '';
}
