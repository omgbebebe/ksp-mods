{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "KerbalAlarmClock-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "TriggerAu";
      repo = "KerbalAlarmClock";
      rev = "3995d43ce4ce2cbf975dbed50085b4fc31d18ad0";
      sha256 = "063na7xvz2v4f3gf87jyynigb74sajp79iqqyklhw6729vd1yib4";
    };
    nativeBuildInputs = [ git mono50 ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
#    patches = [ ./ksp-1.3.0.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
#      rm -f GameData/PersistentRotation/Plugins/PersistentRotation.dll
#      pushd Source
#      csc /noconfig /target:library /langversion:6 /nostdlib+ /checked- /platform:x64 /optimize+ /reference:"${KSP_LIBS}/Assembly-CSharp.dll" /reference:"${KSP_LIBS}/mscorlib.dll" /reference:"${KSP_LIBS}/System.Core.dll" /reference:"${KSP_LIBS}/System.dll" /reference:"${KSP_LIBS}/UnityEngine.dll" /reference:"${KSP_LIBS}/UnityEngine.UI.dll" /out:../GameData/PersistentRotation/Plugins/PersistentRotation.dll /recurse:'*.cs'
#      popd
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= KerbalAlarmClock/KerbalAlarmClock.sln   
#      comm -12 <(ls -1 ./GameData/PersistentRotation/Plugins/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f ./GameData/PersistentRotation/Plugins/%
      mkdir -p GameData/TriggerTech/{Flags,KerbalAlarmClock}
      mkdir -p GameData/TriggerTech/KerbalAlarmClock/{PluginData,Sounds,ToolbarIcons}
      mkdir GameData/TriggerTech/KerbalAlarmClock/PluginData/{Data,Textures}
      cp -av KerbalAlarmClock/bin/Release/KerbalAlarmClock.dll GameData/TriggerTech/KerbalAlarmClock/
      cp -av PlugInFiles/GameData/TriggerTech/KerbalAlarmClock/ToolbarIcons/* GameData/TriggerTech/KerbalAlarmClock/ToolbarIcons/
      cp -av Images/ToolbarIcons-PNG/* GameData/TriggerTech/KerbalAlarmClock/ToolbarIcons/
      cp -av Images/PNG/* GameData/TriggerTech/KerbalAlarmClock/PluginData/Textures/
      cp -av PlugInFiles/GameData/TriggerTech/Flags/* GameData/TriggerTech/Flags/
      cp -av PlugInFiles/GameData/TriggerTech/KerbalAlarmClock/Sounds/* GameData/TriggerTech/KerbalAlarmClock/Sounds/
      cp -av PlugInFiles/GameData/TriggerTech/KerbalAlarmClock/PluginData/Data/* GameData/TriggerTech/KerbalAlarmClock/PluginData/Data/
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/
      cp -av ./GameData/TriggerTech $out/GameData/
    '';
  };
in src
