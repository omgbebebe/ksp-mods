{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "realchute-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "StupidChris";
      repo = "RealChute";
      rev = "6b4e37ccf68b8c9a17f2b668d7518504cf5fe2f3";
      sha256 = "01i7xym1yjgr406la66hbb4419p018aq8kcxf0ayxjwm8knlnzm4";
    };
    nativeBuildInputs = [  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 kspMods.ModularFlightIntegrator ];
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    MFI = "${kspMods.ModularFlightIntegrator}/GameData/ModularFlightIntegrator";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}%3b${MFI}" RealChute.sln /p:PreBuildEvent= /p:PostBuildEvent=
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData
      cp -av Output/GameData/RealChute $out/GameData/
    '';
  };
in src
