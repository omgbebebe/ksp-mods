{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "DeadlyReentry-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "Starwaster";
      repo = "DeadlyReentry";
      rev = "e2999c0ccc133f04209039011abe0cbd02c889e3";
      sha256 = "0vbd7i1vph3x59fryr5ad5a9rs89w5kf9dymrd0yhyix755d11r7";
    };
    nativeBuildInputs = [  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 kspMods.ModularFlightIntegrator ];
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    patches = [ ./csproj.patch ./final.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    MFI = "${kspMods.ModularFlightIntegrator}/GameData/ModularFlightIntegrator";
    buildPhase = ''
      runHook preBuild
      rm -rf DeadlyReentry/Plugins/*
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}%3b${MFI}" /p:PreBuildEvent= /p:PostBuildEvent= Source/DeadlyReentry.sln
      comm -12 <(ls -1 DeadlyReentry/Plugins/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f DeadlyReentry/Plugins/%
      comm -12 <(ls -1 DeadlyReentry/Plugins/) <(ls -1 $MFI) | xargs -I% rm -f DeadlyReentry/Plugins/%
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData
      cp -av DeadlyReentry $out/GameData/
    '';
  };
in src
