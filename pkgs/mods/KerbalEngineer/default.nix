{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "KerbalEngineer-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "CYBUTEK";
      repo = "KerbalEngineer";
      rev = "58bdef1a51d9b6a682db372d25717720f5d4ed8b";
      sha256 = "1p555p1xz81ka8gfqcgrrqv865yxfvngczxgpw58pjf51ycs1mls";
    };
    nativeBuildInputs = [ git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    patches = [ ./fix.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      rm -f Output/KerbalEngineer/*.dll
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= KerbalEngineer.sln
      comm -12 <(ls -1 ./Output/KerbalEngineer/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f ./Output/KerbalEngineer/%
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/
      cp -av ./Output/KerbalEngineer $out/GameData/
    '';
  };
in src
