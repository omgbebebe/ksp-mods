{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "ProceduralParts-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "Swamp-Ig";
      repo = "ProceduralParts";
      rev = "a77ca4b31f25d1911ecfbd156ef20ad5927268de";
      sha256 = "1k237yfc4g7fa1bs1jd40v4m30wh0fw5ipp7nn45kpjzlhrapykq";
    };
    nativeBuildInputs = [  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 kspMods.ModularFlightIntegrator ];
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    patches = [ ./ksp-1.3.0.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      substitute Source/Properties/AssemblyInfo.in Source/Properties/AssemblyInfo.cs \
        --replace '@VERSION@' '1.2.1'

      xbuild /p:Configuration=Release /p:Platform=x86 /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= Source/ProceduralParts.sln
      comm -12 <(ls -1 ./Plugins/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f ./Plugins/%
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/ProceduralParts
      cp -av ./{ModuleManager,Parts,Plugins} $out/GameData/ProceduralParts/
      cp -av ./{README.md,Licence.txt} $out/GameData/ProceduralParts/
    '';
  };
in src
