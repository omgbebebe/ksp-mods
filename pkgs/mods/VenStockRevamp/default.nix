{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "VenStockRevamp-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "VenVen";
      repo = "Stock-Revamp";
      rev = "2a0b419ba3efe1062babaca96b247374fefb979f";
      sha256 = "1rfv8arjd13h0356jdgfnwgpq3nsarcr00v8c20qh9zhsrjk789q";
    };
    nativeBuildInputs = [  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "installPhase" ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    installPhase = ''
      mkdir -p $out/GameData/
      cp -av ./GameData/VenStockRevamp $out/GameData/
    '';
  };
in src
