{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "CustomBarnKit-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "sarbian";
      repo = "CustomBarnKit";
      rev = "9862b585424b887354de6b0d9eaea0ff383a0709";
      sha256 = "0wiwlck30gahlbigdiwvmqygcd7df00hff36jg1ama2v5kmbrnga";
    };
    nativeBuildInputs = [  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
#    patches = [ ./ksp-1.3.0.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      csc /noconfig /target:library /checked- /nowarn:1701,1702,2008 /langversion:6 /nostdlib+ /platform:AnyCPU /warn:4 /errorendlocation /highentropyva- /optimize+ /debug- /filealign:512 /unsafe /reference:${KSP_LIBS}/Assembly-CSharp.dll /reference:${KSP_LIBS}/mscorlib.dll /reference:${KSP_LIBS}/System.Core.dll /reference:${KSP_LIBS}/UnityEngine.dll /reference:${KSP_LIBS}/UnityEngine.UI.dll /out:CustomBarnKit/CustomBarnKit.dll /recurse:'*.cs'
      comm -12 <(ls -1 ./CustomBarnKit/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f ./CustomBarnKit/%
      rm -f ./CustomBarnKit/CustomBarnKit.version.in
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/
      cp -av ./CustomBarnKit $out/GameData/
    '';
  };
in src
