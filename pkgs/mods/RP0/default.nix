{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "RP0-${version}";
    version = "Pap-TechTree";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "KSP-RO";
      repo = "RP-0";
      rev = "774ffe297ab552a364c164670b3c7ef3be769c69";
      sha256 = "1nlrcd9g45ahnspzv6d3xly81mqbx2r7r1hvk7fcvjdysd4y9zhl";
    };
    nativeBuildInputs = [ perlPackages.YAMLLibYAML  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    patches = [ ./ksp-1.3.0.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= Source/RP0.sln
      comm -12 <(ls -1 ./GameData/RP-0/Plugins/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f ./GameData/RP-0/Plugins/%
      [[ -f tree.yml ]] && bin/yml2mm
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/
      cp -av ./GameData/RP-0 $out/GameData/
    '';
  };
in src
