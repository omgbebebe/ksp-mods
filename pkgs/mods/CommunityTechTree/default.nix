{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "CommunityTechTree-${version}";
    version = "dev";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "ChrisAdderley";
      repo = "CommunityTechTree";
      rev = "ba03a2fb906e343fc2853efba3603729dd635d7a";
      sha256 = "154jp9avc1syah95x7y3x4alnh1d5czfli947slwl7c0aynzb4yb";
    };
    nativeBuildInputs = [  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "installPhase" ];
#    patches = [ ./ksp-1.3.0.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    installPhase = ''
      mkdir -p $out/GameData/
      cp -av ./GameData/CommunityTechTree $out/GameData/
    '';
  };
in src
