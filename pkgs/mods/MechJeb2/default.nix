{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "MechJeb2-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "MuMech";
      repo = "MechJeb2";
      rev = "9be3000e57a8140a40831ee3193b9939ab8d9afb";
      sha256 = "1khwzcidq4kxbrz58d2j1sl6hdr59ni1rkl1rp2a6nii2430930d";
    };
    nativeBuildInputs = [  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ];
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      KSPDIR=${KSP_DIR} make package
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData
      cp -av package/MechJeb2 $out/GameData/
    '';
  };
in src
