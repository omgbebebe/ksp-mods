{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "CommunityResourcePack-${version}";
    version = "master";
    src = fetchFromGitHub {
      owner = "BobPalmer";
      repo = "CommunityResourcePack";
      rev = "a51a6e55cf03f112ffb4397b64ca3e41ef7a2812";
      sha256 = "112x39awwwn93s1gaplqzi7fvq0z7lxzcmwgrg4agwy7wv3d5zk0";
    };
    nativeBuildInputs = [ git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "installPhase" ];
    installPhase = ''
      mkdir -p $out/GameData
      cp -av FOR_RELEASE/GameData/CommunityResourcePack $out/GameData/
    '';
  };
in src
