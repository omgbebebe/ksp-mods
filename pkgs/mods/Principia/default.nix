{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

stdenv.mkDerivation rec {
  name = "Principia-${version}";
  version = "master";
  src = fetchFromGitHub {
    owner = "mockingbirdnest";
    repo = "Principia";
    rev = "1ac7ad49fff7d92a6d1d113cdc155ebb0df5dfe2";
    sha256 = "0dahhsvkvibbzwvdgx76fhhb2kk3m42pysy0sh0r79xk8mnipgal";
  };

  nativeBuildInputs = [ git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
  buildInputs = [ ksp130 protobuf3_2 libcxxabi libcxx ];
  phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
  patches = [ ./nix_build.patch ./ksp-1.3.patch ./shallow_clone.patch ];
  KSP_DIR = "${ksp130}";
  preBuild = ''
    git init
    git config user.email "you@example.com"
    git config user.name "Your Name"
    git add .
    git commit -m 'initial'
    mkdir -p deps/
    export GIT_SSL_CAINFO=/etc/ssl/certs/ca-certificates.crt
    export SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt
    substituteInPlace ./install_deps.sh --replace /bin/bash ${bash}/bin/bash
    substituteInPlace ./generate_version_translation_unit.sh --replace /bin/bash ${bash}/bin/bash
    ./install_deps.sh
  '';

  buildPhase = ''
    runHook preBuild
    make release
    runHook postBuild
  '';

  installPhase = ''
    mkdir -p $out/GameData
    cp -av Release/GameData/Principia $out/GameData/
  '';
}
