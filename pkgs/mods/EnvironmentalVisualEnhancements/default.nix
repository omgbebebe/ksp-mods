{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "EnvironmentalVisualEnhancements-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "WazWaz";
      repo = "EnvironmentalVisualEnhancements";
      rev = "204cbd6a8f9a6ceb096e27d6966e7bd6a6a93308";
      sha256 = "1skl1mh1i520zljp5jiqn8jjhk5ib9r4kbd1vzz6msc6b2cydmxx";
    };
    nativeBuildInputs = [  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    patches = [ ./fix.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      mkdir ContentEVE/GameData/EnvironmentalVisualEnhancements/Plugins/
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" EnvironmentalVisualEnhancements.sln
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/
      cp -av ./ContentEVE/GameData/EnvironmentalVisualEnhancements $out/GameData/
      echo "NEED TO COMPILE SHADERS BUNDLE SOMEHOW!!!111"
    '';
  };
in src
