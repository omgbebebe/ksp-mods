{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "SmokeScreen-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "sarbian";
      repo = "SmokeScreen";
      rev = "bbd61da225eef6c29d9e692afb1f23f4df2397c4";
      sha256 = "0566j3ksbfzg1ldgpg6pzj19wlc9msji0slvzvi9is4xjikxa2p3";
    };
    nativeBuildInputs = [ git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
#    patches = [ ./ksp-1.3.0.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      csc /noconfig /target:library /langversion:6 /nostdlib+ /checked- /platform:x64 /optimize+ /reference:"${KSP_LIBS}/Assembly-CSharp.dll" /reference:"${KSP_LIBS}/mscorlib.dll" /reference:"${KSP_LIBS}/System.Core.dll" /reference:"${KSP_LIBS}/System.dll" /reference:"${KSP_LIBS}/UnityEngine.dll" /reference:"${KSP_LIBS}/UnityEngine.UI.dll" /out:GameData/SmokeScreen/SmokeScreen.dll /recurse:'*.cs'
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData
      cp -av GameData/SmokeScreen $out/GameData/
    '';
  };
in src
