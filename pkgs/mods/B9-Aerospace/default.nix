{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "B9-Aerospace-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "blowfishpro";
      repo = "B9-Aerospace";
      rev = "b2e6dd9577bcab6b40ba870aa8ec70a22ec41526";
      sha256 = "0ck9yjm4cmb4sb1r5brqgbags3jyq2vyj38ghnhkmmf6hfdnqsnd";
    };
    nativeBuildInputs = [ git mono50 curl unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= AGExt.sln
      
      mkdir -p ./GameData/AGExt
      cp -av ./AGExt/obj/Release/AGExt.dll ./GameData/AGExt/
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/Diazo
      cp -av ./GameData/AGExt $out/GameData/Diazo/
      cp -av ${kspMods.AGExtData}/GameData/Diazo/AGExt/* ./GameData/AGExt $out/GameData/Diazo/AGExt/
    '';
  };
in src
