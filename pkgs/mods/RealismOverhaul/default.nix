{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "realismoverhaul-${version}";
    version = "pap1723-ROupdates";
    src = fetchFromGitHub {
      owner = "KSP-RO";
      repo = "RealismOverhaul";
      rev = "63bca9bc3ac643ab74bca687229afc1a42807ff5";
      sha256 = "0k4gr5m3zg3yird13dky6fb6xrhwb9ivg51d84msabyhbdr5y34j";
    };
    nativeBuildInputs = [ git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 kspMods.ModularFlightIntegrator ];
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    patches = [ ./build-1.3.0.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    MFI = "${kspMods.ModularFlightIntegrator}/GameData/ModularFlightIntegrator";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}%3b${MFI}" /p:PreBuildEvent= /p:PostBuildEvent= Source/RealismOverhaul.sln
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}%3b${MFI}" /p:PreBuildEvent= /p:PostBuildEvent= Source/KerbalPlugins.EngineGroupController.sln
      comm -12 <(ls -1 GameData/EngineGroupController/Plugins/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f GameData/EngineGroupController/Plugins/%
      comm -12 <(ls -1 GameData/RealismOverhaul/Plugins/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f GameData/RealismOverhaul/Plugins/%
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData
      cp -av GameData/RealismOverhaul $out/GameData/
      cp -av GameData/EngineGroupController $out/GameData/
    '';
  };
in src
