{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "Firespitter-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "snjo";
      repo = "Firespitter";
      rev = "f1fda0f394be3a6e9220316a905416be4c54c2c3";
      sha256 = "0xajqvfq5scpflcgdcvz356ipdarskpyxay8sq70lclmcbdp2z5n";
    };
    nativeBuildInputs = [  git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ];
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      rm -f "For release/Firespitter/Plugins/*"
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= Firespitter.sln
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData
      cp -av "For release/Firespitter" $out/GameData/
    '';
  };
in src
