{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "solverengines-${version}";
    version = "master";
    src = fetchFromGitHub {
      owner = "KSP-RO";
      repo = "SolverEngines";
      rev = "ef06c0e7074a7bdcd63a01982e475640137e30ac";
      sha256 = "0qmdp3czq14bm0q9d3hc3si3g6pl2i6lkcmp8qh7jga6i5a5smai";
    };
    nativeBuildInputs = [ git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ];
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= SolverEngines/SolverEngines.sln
      comm -12 <(ls -1 GameData/SolverEngines/Plugins/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f GameData/SolverEngines/Plugins/%
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData
      cp -av GameData/SolverEngines $out/GameData/
    '';
  };
in src
