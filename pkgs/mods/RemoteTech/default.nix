{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "RemoteTech-${version}";
    version = "develop";
    src = fetchFromGitHub {
      owner = "RemoteTechnologiesGroup";
      repo = "RemoteTech";
      rev = "66f5f2654696dab16e4415a36f35f08c3322cb7e";
      sha256 = "1hbyhcf06iww5737bv5pks1rqgsglkafi20yq1b82v4d437zg0f9";
    };
    nativeBuildInputs = [ git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
#    patches = [ ./build-1.3.0.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= src/RemoteTech/RemoteTech.sln
      comm -12 <(ls -1 GameData/RemoteTech/Plugins/) <(ls -1 $KSP_LIBS) | xargs -I% rm -f GameData/RemoteTech/Plugins/%
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData
      cp -av GameData/RemoteTech $out/GameData/
    '';
  };
in src
