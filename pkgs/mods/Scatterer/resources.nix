{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "Scatter-Resources-${version}";
    version = "0.300";
    src = fetchurl {
      url = "https://spacedock.info/mod/141/scatterer/download/0.0300";
      sha256 = "0v6hqm5qbp21k4cjkp5mpx0sq0kkga6wfq7ycnaa5ic4zg724nwg";
    };
    nativeBuildInputs = [ unzip ];
    phases = [ "unpackPhase" "installPhase" ];
    unpackPhase = ''
      unzip $src
      find ./ -name '*.dll' -delete
    '';
    installPhase = ''
      mkdir -p $out/GameData
      cp -av ./GameData/scatterer $out/GameData/
    '';
  };
in src
