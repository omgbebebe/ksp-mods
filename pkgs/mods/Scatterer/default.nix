{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "Scatterer-${version}";
    version = "master";
    src = fetchFromGitHub {
      fetchSubmodules = true;
      owner = "LGhassen";
      repo = "Scatterer";
      rev = "16541bb1b06663b4ba54cf26aa49410851ec198b";
      sha256 = "010x4db4775926fyxw4k3dfjcnypr4czfms5k6fhd6gq09dwssdn";
    };
    nativeBuildInputs = [ git mono50 cacert clang_4 which cmake curl autoconf automake perl pkgconfig libtool unzip ];
    buildInputs = [ ksp130 ] ++ (with kspMods; [
      Scatterer-Resources
    ]);
    phases = [ "unpackPhase" "patchPhase" "buildPhase" "installPhase" ];
    patches = [ ./fix.patch ];
    KSP_DIR = "${ksp130}";
    KSP_LIBS = "${KSP_DIR}/KSP_Data/Managed";
    buildPhase = ''
      runHook preBuild
      xbuild /p:Configuration=Release /p:ReferencePath="${KSP_LIBS}" /p:PreBuildEvent= /p:PostBuildEvent= scatterer/scatterer.sln
      runHook postBuild
    '';
    installPhase = ''
      mkdir -p $out/GameData/scatterer
      cp -av ${kspMods.Scatterer-Resources}/GameData/scatterer/* $out/GameData/scatterer/
      cp -av ./scatterer/obj/Release/scatterer.exe $out/GameData/scatterer/scatterer.dll
    '';
  };
in src
