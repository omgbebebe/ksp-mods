{ pkgs ? import <nixpkgs> {}, stdenv ? pkgs.stdenv }:

with pkgs;

let
  src = stdenv.mkDerivation rec {
    name = "RealPlume-${version}";
    version = "master";
    src = fetchFromGitHub {
      owner = "KSP-RO";
      repo = "RealPlume";
      rev = "9fb04267b8c35bc5894f36957774b8608b090577";
      sha256 = "18kd3lpp8c9fpz27yrplgxzzjkrw4xl69lbj5p0q39h47iqd1hxh";
    };
    buildInputs = [ ksp130 ] ++ (with kspMods; [
      ModularFlightIntegrator
    ]);
    phases = [ "unpackPhase" "installPhase" ];
    installPhase = ''
      mkdir -p $out/GameData
      cp -av GameData/RealPlume $out/GameData/
    '';
  };
in src
