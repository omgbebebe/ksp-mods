{ pkgs ? import <nixpkgs> {}, stdenv, callPackage, fetchFromGitHub, fetchurl }:

let
  inherit (pkgs) fetchFromGitHub unzip buildDotnetPackage;
  inherit (stdenv.lib) toList makeOverridable;
  mono = pkgs.mono46;

  #kspMod = makeOverridable (callPackage ./pkgs/build-support/build-ksp-module.nix { } );
  kspMod = callPackage ./pkgs/build-support/build-ksp-module.nix { };

  #_ksp = { version, url, sha256}: callPackage ./pkgs/ksp {inherit url; inherit version; inherit sha256; };
  _ksp = callPackage ./pkgs/ksp {};

  _ksp122 = _ksp {
    version = "1.2.2";
    url = "file://---use_nix-prefetch---/gog_kerbal_space_program_2.12.0.15.sh";
    sha256 = "1j1b9d2j2g3vvc5hcyrx9ablfj21bw3hszp4ys02gjw11hbwcqyj";
  };
/*
  _ksp130 = _ksp {
    version = "1.3.0";
    url = "file://---use_nix-prefetch---/gog_kerbal_space_program_3.12.0.15.sh";
    sha256 = "2j1b9d2j2g3vvc5hcyrx9ablfj21bw3hszp4ys02gjw11hbwcqyj";
  };
*/

  _withMods = {mods}: stdenv.mkDerivation rec {
    name = "ksp-with-mods";
    phases = [ "installPhase" ];
    buildInput = [ pkgs.rsync ];
    nativeBuildInputs = with pkgs.kspMods; [
      ModuleManager
    ] ++ mods;
    DEPS = toList nativeBuildInputs;

    installPhase = ''
      echo "installing KSP-with-mods"
      mkdir $out
      for d in $DEPS; do
        ${pkgs.rsync}/bin/rsync -a --ignore-existing $d/* $out/
      done
    '';
  };

in rec {

# TODO: add impureEnvVars = [ "KSPDIR" ]; 

ksp = ksp122;
ksp122 = _ksp122;

withMods = _withMods { mods = with pkgs.kspMods; [ 
  Kopernicus
  MechJeb
  (RealSolarSystem { texRes = "4K"; })
];};

ModularFlightIntegrator = callPackage ./pkgs/mods/ModularFlightIntegrator { };
Kopernicus = callPackage ./pkgs/mods/Kopernicus { };
RealSolarSystem = callPackage ./pkgs/mods/RealSolarSystem { };
RSS-Textures = callPackage ./pkgs/mods/RealSolarSystem/textures.nix {};
Principia = callPackage ./pkgs/mods/Principia { };
FerramAerospaceResearch = callPackage ./pkgs/mods/FerramAerospaceResearch {};
FAR = FerramAerospaceResearch;
SolverEngines = callPackage ./pkgs/mods/SolverEngines {};
AJE = callPackage ./pkgs/mods/AJE {};
RemoteTech = callPackage ./pkgs/mods/RemoteTech {};
KerbalKonstructs = callPackage ./pkgs/mods/KerbalKonstructs {};
ContractConfigurator = callPackage ./pkgs/mods/ContractConfigurator {};
TestFlight = callPackage ./pkgs/mods/TestFlight {} ;
RealFuels = callPackage ./pkgs/mods/RealFuels {};
RealismOverhaul = callPackage ./pkgs/mods/RealismOverhaul {};
CommunityResourcePack = callPackage ./pkgs/mods/CommunityResourcePack {};
KerbalJointReinforcement = callPackage ./pkgs/mods/KerbalJointReinforcement {};
RealChute = callPackage ./pkgs/mods/RealChute {};
RealHeat = callPackage ./pkgs/mods/RealHeat {};
RealPlume = callPackage ./pkgs/mods/RealPlume {};
SmokeScreen = callPackage ./pkgs/mods/SmokeScreen {};
B9-PWings-Fork = callPackage ./pkgs/mods/B9-PWings-Fork {};
ConnectedLivingSpace = callPackage ./pkgs/mods/ConnectedLivingSpace {};
DeadlyReentry = callPackage ./pkgs/mods/DeadlyReentry {};
FilterExtension = callPackage ./pkgs/mods/FilterExtension {};
MechJeb2 = callPackage ./pkgs/mods/MechJeb2 {};
ProceduralFairings = callPackage ./pkgs/mods/ProceduralFairings {};
ProceduralFairings-ForEverything = callPackage ./pkgs/mods/ProceduralFairings-ForEverything {};
Firespitter = callPackage ./pkgs/mods/Firespitter {};
ProceduralParts = callPackage ./pkgs/mods/ProceduralParts {};
RCSBuildAid = callPackage ./pkgs/mods/RCSBuildAid {};
Toolbar = callPackage ./pkgs/mods/Toolbar {};
RW-Saturatable = callPackage ./pkgs/mods/RW-Saturatable {};
TacLifeSupport = callPackage ./pkgs/mods/TacLifeSupport {};

# RP-0 stuff
RP0 = callPackage ./pkgs/mods/RP0 {};
CommunityTechTree = callPackage ./pkgs/mods/CommunityTechTree {};
SXTContinued = callPackage ./pkgs/mods/SXTContinued {};
RetractableLiftingSurface = callPackage ./pkgs/mods/RetractableLiftingSurface {};
VenStockRevamp = callPackage ./pkgs/mods/VenStockRevamp {};
CustomBarnKit = callPackage ./pkgs/mods/CustomBarnKit {};
KerbalConstructionTime = callPackage ./pkgs/mods/KerbalConstructionTime {};
MagiCore = callPackage ./pkgs/mods/MagiCore {};
Taerobee = callPackage ./pkgs/mods/Taerobee {};

# useful stuff
X-Science = callPackage ./pkgs/mods/X-Science {}; # maybe need to fix modname (wav didn't playing)
ForScience = callPackage ./pkgs/mods/ForScience {};

# visuals
# TODO: need to compile shaders by self or get it somehow from binaries
RSSVE = callPackage ./pkgs/mods/RSSVE {};
EnvironmentalVisualEnhancements = callPackage ./pkgs/mods/EnvironmentalVisualEnhancements {};
Scatterer = callPackage ./pkgs/mods/Scatterer {};
Scatterer-Resources = callPackage ./pkgs/mods/Scatterer/resources.nix {};

# libs
REPOSoftTechKSPUtils = callPackage ./pkgs/mods/REPOSoftTechKSPUtils {};
ksp130 = callPackage ./pkgs/ksp-libs {};

PersistentRotation = callPackage ./pkgs/mods/PersistentRotation {};
KerbalAlarmClock = callPackage ./pkgs/mods/KerbalAlarmClock {};

# fun stuff
FMRS = callPackage ./pkgs/mods/FMRS {};
StageRecovery = callPackage ./pkgs/mods/StageRecovery {};
Chatterer = callPackage ./pkgs/mods/Chatterer {};
ChattererResources = callPackage ./pkgs/mods/Chatterer/resources.nix {};

# helpers
EditorExtensionsRedux = callPackage ./pkgs/mods/EditorExtensionsRedux {};
KerbalEngineer = callPackage ./pkgs/mods/KerbalEngineer {};

AGExt = callPackage ./pkgs/mods/AGExt {};
AGExtData = callPackage ./pkgs/mods/AGExt/pluginData.nix {};

HyperEdit = callPackage ./pkgs/mods/HyperEdit {};

ModuleManager = callPackage ./pkgs/mods/ModuleManager {};
ScrapYard = callPackage ./pkgs/mods/ScrapYard {};
KSP_Contract_Window = callPackage ./pkgs/mods/KSP_Contract_Window {};

# meta mods
RP0-Pap-TechTree = callPackage ./pkgs/RP0-Pap-TechTree {};
TestMeta = callPackage ./pkgs/TestMeta {};
}
