## Configure overlay

```sh
ln -s $(pwd)/ksp-mods.nix ~/.config/nixpkgs/overlays/
```

## Install

```sh
nix-build --show-trace '<nixpkgs>' -A kspMods.Kopernicus
```
