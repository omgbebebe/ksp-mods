#!/usr/bin/env bash

set -euo pipefail
#set -x

DATE_NOW=$(date +%y%m%d%H%M%S)
UPDATED=0
MODS=$(find ./pkgs/mods/ -name 'default.nix' | xargs -I% awk -F"=" '{if ($1 ~ /rev/) rev=gensub(/[\"; \t]+/, "","g",$2)}{if ($1 ~ /version/) v=gensub(/[\"; \t]+/, "","g",$2)}{if ($1 ~ /owner/) p1=gensub(/[\";\t ]+/, "","g",$2)}{ if ($1 ~ /repo/) p2=gensub(/[\";\t ]+/, "","g",$2)}{ if (p1 && p2 && v && rev) {print FILENAME","p1","p2","v","rev;v=p1=p2=rev=""}}' %)

rm -f ./cmd
for m in $MODS;do
  owner=$(echo $m | cut -d',' -f2)
  repo=$(echo $m | cut -d',' -f3)
  branch=$(echo $m | cut -d',' -f4)
  orev=$(echo $m | cut -d',' -f5)
  file=$(echo $m | cut -d',' -f1)
  echo $file

  rev=$(git ls-remote -h git://github.com/$owner/$repo | grep -e "heads/$branch\$" | awk '{print $1}')
  [[ $rev == $orev ]] && continue
  echo "Updating $owner/$repo"
  UPDATED=1

  hash=$(nix-prefetch-git --url https://github.com/${owner}/${repo}.git --rev ${rev} --no-deepClone --fetch-submodules 2>&1 | grep 'hash is ' | cut -d' ' -f3)

  sed -i "s/rev = \(.*\);/rev = \"$rev\";/" $file
  sed -i "s/sha256 = \(.*\);/sha256 = \"$hash\";/" $file
  echo "$repo:$rev:$hash"
  echo git add $file >> ./cmd
  echo git commit -m "'auto update $owner/$repo'" >> ./cmd
done

if [[ $UPDATED -ne 0 ]]; then
  git checkout -b update-$DATE_NOW
  chmod +x ./cmd && ./cmd
  rm -f ./cmd
fi
