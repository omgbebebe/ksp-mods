self: super:

let 
  self =
{
  ksp122 = self.kspMods.ksp;
  ksp130 = self.kspMods.ksp130;
  Principia = self.kspMods.Principia;
  #ksp122 = self.kspMods.withMods {};
  #ksp = super.callPackage ./pkgs/ksp {};

  #kspMods = super.callPackage ./default.nix { inherit (super) callPackage; inherit self; } { };
  #kspMods = super.callPackage ./default.nix { inherit (super) callPackage; inherit self; } { };
  kspMods = super.callPackage ./default.nix { };
};

in self
