#!/usr/bin/env bash

TMPDIR=$(mktemp -d)
tar -czf ${TMPDIR}/ksp-libs-1.3.0.tar.gz --mtime='1970-01-01' -C KSP_Data/Managed/ -T $(dirname "$0")/ksp-libs-1.3.0.list
nix-prefetch-url file://${TMPDIR}/ksp-libs-1.3.0.tar.gz
rm -rf $TMPDIR
