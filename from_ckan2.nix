{ pkgs ? import <nixpkgs> {} }:

with pkgs;

let
  ckanMetaRoot = "/home/scor/work/dev/ksp/CKAN-meta";
  modlist = builtins.attrNames (builtins.readDir "${ckanMetaRoot}/");
  ckan-mods =  builtins.listToAttrs (map (p: {name = p; value = fromCKAN { modName = p; kspVer = "1.2.2";};}) modlist);
  mm = {mn, kspVer}:
    let
      modName = mn;
      files = builtins.attrNames (builtins.readDir "${ckanMetaRoot}/${modName}/");
      ckans = builtins.filter (f: (builtins.match ".*ckan" f) != null) files;
      mods  = map (f: builtins.fromJSON ( builtins.readFile "${ckanMetaRoot}/${modName}/${f}")) ckans;
#      mod = builtins.foldl' (a: m: if (builtins.compareVersions kspVer m.ksp_version) == 0 then m else a) null mods;
      mod = builtins.foldl' (a: m:
        let v = if builtins.hasAttr "ksp_version" m then m.ksp_version
                else if builtins.hasAttr "ksp_max_version" m then m.ksp_max_version
                else "1.2.2";
        in
          if (builtins.compareVersions kspVer v) == 0
          then m
          else a ) null mods;

    in mod;

  fromCKAN = {modName, kspVer}: let
    mod = mm {mn = modName; inherit kspVer;};
    in if mod == null then throw "${modName} not found in ${ckanMetaRoot}/${modName}" else
      stdenv.mkDerivation rec {
        name = builtins.replaceStrings [":"] ["-"] "${mod.identifier}-${mod.version}";
        src = fetchurl {
          url = mod.download;
          sha256 = mod.download_hash.sha256;
        };
        deps = if builtins.hasAttr "depends" mod then map (x: x.name) mod.depends else [];
        mdeps = map (x: ckan-mods.${x}) (builtins.filter (x: x != "RealFuels-Engine-Configs" && x != "RealPlumeConfigs" ) (map (x: x.name) (if builtins.hasAttr "depends" mod then mod.depends else [])));
        buildInputs = mdeps;
        FROM =
          if builtins.hasAttr "install" mod then
            if builtins.hasAttr "find" (builtins.head mod.install)
            then (builtins.head mod.install).find
            else if builtins.hasAttr "file" (builtins.head mod.install)
              then (builtins.head mod.install).file
              else "*.dll"
          else "${mod.identifier}";
        TO   =
          if builtins.hasAttr "install" mod
          then (builtins.head mod.install).install_to
          else "GameData/";
        unpackPhase = ''${unzip}/bin/unzip -qq $src'';
        installPhase = ''
          mkdir $out; touch $out/.empty
          mkdir -p $out/$TO
          ls -la ./
          if [[ -d $FROM ]]; then
            cp -rv $FROM $out/$TO
          elif [[ -d GameData/$FROM ]]; then
            cp -rv GameData/$FROM $out/$TO
          elif find ./ -maxdepth 2 -type d -name "$FROM" -print -quit | egrep '.*' ;then
            F=$(find ./ -maxdepth 2 -type d -name "$FROM" -print -quit)
            cp -rv $F $out/$TO
          else
            cp -rv $FROM $out/$TO
          fi

          for m in $nativeBuildInputs; do
            ln -s $m/GameData/* $out/GameData/ || true
          done

        '';
      };

in
  builtins.listToAttrs (map (p: {name = p; value = fromCKAN { modName = p; kspVer = "1.2.2";};}) modlist)
